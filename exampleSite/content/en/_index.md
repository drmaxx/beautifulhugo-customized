## Beautifulhugo Customized

This is a customization of the Beautiful Hugo Theme and an attempt to add most of the enhancements I have found so far, in one Theme.

Credit to [halogenica](https://github.com/halogenica/beautifulhugo) for porting the Theme to Hugo, and [daattali](https://github.com/daattali/beautiful-jekyll) for creating the original Jekyll Theme.

## Front Page Content
`beautifulhugo-customized` supports content on your front page. Edit `/content/_index.md` to change what appears here. Delete `/content/_index.md` if you don't want any content here.

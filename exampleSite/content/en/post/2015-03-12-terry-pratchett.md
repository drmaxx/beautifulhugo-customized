---
title: "Terry Pratchett"
date: 2015-12-03
subtitle: "GNU Terry Pratchett"
image: "https://upload.wikimedia.org/wikipedia/commons/7/7a/10.12.12TerryPratchettByLuigiNovi1.jpg"
tags: ["example", "Terry Pratchett"]
---

From [Wikipedia](https://en.wikipedia.org/wiki/Terry_Pratchett)

Sir Terence David John Pratchett OBE (28 April 1948 - 12 March 2015) was an English humorist, satirist, and author of fantasy novels, especially comical works. He is best known for his Discworld series of 41 novels.

Pratchett's first novel, _The Carpet People_, was published in 1971. The first Discworld novel, _The Colour of Magic_, was published in 1983, after which Pratchett wrote an average of two books a year. His 2011 Discworld novel _Snuff_ became the third-fastest-selling hardback adult-readership novel since records began in the UK, selling 55.000 copies in the first three days. The final Discworld novel, _The Shepherd's Crown_, was published in August 2015, five months after his death.
<!--more-->
Pratchett, with more than 85 million books sold worldwide in 37 languages, was the UK's best-selling author of the 1990s. He was appointed Officer of the Order of the British Empire (OBE) in 1998 and was knighted for services to literature in the 2009 New Year Honours. In 2001 he won the annual Carnegie Medal for _The Amazing Maurice and his Educated Rodents_, the first Discworld book marketed for children. He received the World Fantasy Award for Life Achievement in 2010.

In December 2007, Pratchett announced that he had been diagnosed with early-onset Alzheimer's disease. He later made a substantial public donation to the Alzheimer's Research Trust (now Alzheimer's Research UK), filmed a television programme chronicling his experiences with the condition for the BBC, and became a patron for Alzheimer's Research UK. Pratchett died on 12 March 2015, aged 66.

![Terry Pratchett](https://upload.wikimedia.org/wikipedia/commons/7/7a/10.12.12TerryPratchettByLuigiNovi1.jpg)

---
title: About me
subtitle: Have you seen my Luggage?
comments: false
---

Hello my Name is Twoflower.

I'm a native of the Agatean Empire, on the Counterweight Continent, where I work as an inn-sewer-ants clerk, and I'm the first tourist ever on the Discworld. I wrote the bestseller "What I Did on My Holidays".

I visited the city of Ankh-Morpork, where I meets the inept wizard Rincewind and hired him as a guide. I was followed by the Luggage, a homicidally vicious travel chest which moves on hundreds of little legs, carrying my belongings.

### My Travels

If you wan't to know more about my travels, I'd suggest you [read](https://en.wikipedia.org/wiki/The_Colour_of_Magic) or [watch](https://www.imdb.com/title/tt1079959/) "_The Colour of Magic._"

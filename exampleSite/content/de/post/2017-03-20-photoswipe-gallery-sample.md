---
title: Photoswipe Galerie Beispiel
subtitle: Erstellung einer Galerie
date: 2017-03-20
tags: ["example", "photoswipe"]
---

Beautifulhugo Customized beinhaltet ein paar Shortcodes von [Li-Wen Yip](https://www.liwen.id.au/heg/) und [Gert-Jan van den Berg](https://github.com/GjjvdBurg/HugoPhotoSwipe), um eine Galerie mit [PhotoSwipe](http://photoswipe.com) zu erstellen.

{{< gallery caption-effect="fade" >}}
  {{< figure thumb="-thumb" link="/img/hexagon.jpg" >}}
  {{< figure thumb="-thumb" link="/img/sphere.jpg" caption="Kugeln" >}}
  {{< figure thumb="-thumb" link="/img/triangle.jpg" caption="Dreieck" alt="Dies ist ein langer Kommentar über ein Dreieck" >}}
{{< /gallery >}}

<!--more-->
## Beispiel
Die Galerie oben wurde mit folgendem shortcode erstellt:
```
{{</* gallery caption-effect="fade" */>}}
  {{</* figure thumb="-thumb" link="/img/hexagon.jpg" */>}}
  {{</* figure thumb="-thumb" link="/img/sphere.jpg" caption="Kugeln" */>}}
  {{</* figure thumb="-thumb" link="/img/triangle.jpg" caption="Dreieck" alt="Dies ist ein langer Kommentar über ein Dreieck" */>}}
{{</* /gallery */>}}
```

## Nutzung
Für alle Details, schau dir bitte die [hugo-easy-gallery GitHub](https://github.com/liwenyip/hugo-easy-gallery/) Seite an. Grundlagen vom Beispiel oben sind:

- Beginne und ende eine Galerie mit den Tags `{{</* gallery */>}}` und `{{</* /gallery */>}}`
- `{{</* figure src="image.jpg" */>}}` wird `image.jpg` für Vorschau- und voll-Bild nutzen
- `{{</* figure src="thumb.jpg" link="image.jpg" */>}}` benutzt `thumb.jpg` Für die Vorschau und `image.jpg` für das volle Bild
- `{{</* figure thumb="-small" link="image.jpg" */>}}` wird `image-small.jpg` für die Vorschau und `image.jpg` für das volle Bild nutzen
- Alle [Features / Parameter](https://gohugo.io/extras/shortcodes) von Hugo's eingebautem `figure` shortcode funktionieren wie gewohnt, z.B. src, link, title, caption, class, attr (attribution), attrlink, alt
- `{{</* gallery caption-effect="fade" */>}}` wird die Elemente einblenden anstatt dem üblichen slide-up Effekt
- Viele verschiedenen Stile für Beschreibungen und Hover Effekte sind verfügbar,  schau dir [hugo-easy-gallery auf GitHub](https://github.com/liwenyip/hugo-easy-gallery/) für alle Optionen an
- Beachte das dieses Theme die photoswipe Galerie und Skripte automatisch lädt, photoswipe muss nicht zusätzlich geladen werden

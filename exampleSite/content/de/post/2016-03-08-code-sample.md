---
title: Code Beispiele
subtitle: Benutze Hugo oder Pygments
date: 2016-03-08
tags: ["example", "code"]
---

Hier folgen zwei Code Beispiele mit Syntax Highlighting.

<!--more-->

Das folgende Code Beispiel benutzt den dreifache Backtick ( ` ``` ` ) von Hugo zum Highlighting. Dies ist ein Clientseitiges Highlighting und bedarf keiner speziellen Einstellungen.

```javascript
var num1, num2, sum
num1 = prompt("Enter first number")
num2 = prompt("Enter second number")
sum = parseInt(num1) + parseInt(num2) // "+" means "add"
alert("Sum = " + sum)  // "+" means combine into a string
```

Das folgende Beispiel benutzt den `highlight` Shortcode, mitgeliefert in Hugo. Dies ist ein Serverseitiges Highlighting und benötigt installiertes Python und Pygments, erlaubt jedoch wesentlich mehr Optionen.

{{< highlight javascript >}}
var num1, num2, sum
num1 = prompt("Enter first number")
num2 = prompt("Enter second number")
sum = parseInt(num1) + parseInt(num2) // "+" means "add"
alert("Sum = " + sum)  // "+" means combine into a string
{{</ highlight >}}


Und hier noch ein Beispiel mit Zeilennummern und Tabelle:

{{< highlight javascript "linenos=table">}}
var num1, num2, sum
num1 = prompt("Enter first number")
num2 = prompt("Enter second number")
sum = parseInt(num1) + parseInt(num2) // "+" means "add"
alert("Sum = " + sum)  // "+" means combine into a string
{{</ highlight >}}

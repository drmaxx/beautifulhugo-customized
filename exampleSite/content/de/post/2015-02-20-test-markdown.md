---
title: Test markdown
subtitle: Jeder Post hat auch einen Untertitel
date: 2015-02-20
tags: ["example", "markdown", "code"]
---

Du kannst reguläres [markdown](http://markdowntutorial.com/) benutzen und [Hugo](https://gohugo.io) wird dies automatisch in eine hübsche Webseite umwandeln. Ich möchte empfehlen sich [5 Minuten zeit zu nehmen zu lernen wie man Markdown nutzt](http://markdowntutorial.com/) - hier lernt man normalen Text in Fett / Kursiv / Überschriften / Tabellen und Co zu verwandeln.

**Hier ist ein fetter Text**

## Dies ist eine Überschrift 2. Grades

Hier ist eine nutzlose Tabelle:

| Nummer | Nächste Nummer | Vorherige Nummer |
| :----- | :------------- | :--------------- |
| Fünf   | Sechs          | Vier             |
| Zehn   | Elf            | Neun             |
| Sieben | Acht           | Sechs            |
| Zwei   | Drei           | Einz             |

Wie wäre es mit einem leckeren Crepe?

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

Hier ist etwas Code mit Syntax Highlighting:

```java
var foo = function(x) {
  return(x + 5);
}
foo(3)
```

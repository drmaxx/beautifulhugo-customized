---
title: Große Header Beispiele
subtitle: Nutze multiple Bilder
date: 2017-03-07
tags: ["example", "bigimg", "code"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

Die Bilder oben auf der Seite werden im diesem Theme `bigimg` genannt. Sie sind optional und es können mehrere auf einmal spezifiziert werden. Wenn mehr als ein Bild aufgeführt ist, wechseln diese alle 10 Sekunden. In der front matter werden `bigimg` als array aufgelistet. 

<!--more-->

Ein einzelnes `bigimg` kann in der front matter wie folgt definiert werden:
```yaml
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}]
```

Mehrere `bigimg` werden so bestimmt:
```yaml
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, 
         {src: "/img/sphere.jpg", desc: "Sphere"}, 
         {src: "/img/hexagon.jpg", desc: "Hexagon"}]
```

Das Beschreibungsfeld ist optional, Bilder können auch so bestimmt werden:
```yaml
bigimg: [{src: "/img/triangle.jpg"}, 
         {src: "/img/sphere.jpg"}, 
         {src: "/img/hexagon.jpg"}]
```

Das YAML sniplet oben wurde im "flow" Stil geschrieben. Natürlich kann auch der folgende Stil genutzt werden:

```yaml
bigimg: 
- {src: "/img/triangle.jpg", desc: "Triangle"}
- {src: "/img/sphere.jpg", desc: "Sphere"}
- {src: "/img/hexagon.jpg", desc: "Hexagon"}
```

Weitere Informationen könne in diesem [YAML tutorial](https://rhnh.net/2011/01/31/yaml-tutorial/) gefunden werden.

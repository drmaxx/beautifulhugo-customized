# Beautifulhugo Customized - An adaptation of the Beautiful Jekyll theme

Credit to [halogenica](https://github.com/halogenica/beautifulhugo) for porting the Theme to Hugo, and [daattali](https://github.com/daattali/beautiful-jekyll) for creating the original.

This is just my personal customization and an attempt to add most of the enhancements I have found so far, in one repo.

Demo can be found [here](https://beautifulhugo-customized.drmaxx.org).

[![Beautifulhugo Customized Theme Screenshot](images/screenshot.png)](https://beautifulhugo-customized.drmaxx.org)

---

<!-- MDTOC maxdepth:6 firsth1:2 numbering:0 flatten:0 bullets:1 updateOnSave:1 -->

- [Installation](#Installation)   
   - [Installation](#Installation-1)   
   - [Install as submodule](#Install-as-submodule)   
- [Extra Features](#Extra-Features)   
   - [Responsive](#Responsive)   
   - [Syntax highlighting](#Syntax-highlighting)   
      - [Chroma - New server side syntax highlighting](#Chroma---New-server-side-syntax-highlighting)   
      - [Pygments - Old server side syntax highlighting](#Pygments---Old-server-side-syntax-highlighting)   
      - [Highlight.js - Client side syntax highlighting](#Highlightjs---Client-side-syntax-highlighting)   
   - [Disqus support](#Disqus-support)   
   - [Staticman support](#Staticman-support)   
   - [Google Analytics](#Google-Analytics)   
   - [Commit SHA on the footer](#Commit-SHA-on-the-footer)   
   - [Multilingual](#Multilingual)   
   - [Extra shortcodes](#Extra-shortcodes)   
      - [Details](#Details)   
      - [Split](#Split)   
- [About](#About)   
- [License](#License)   

<!-- /MDTOC -->

---

## Installation

### Installation

See [the Hugo documentation](https://gohugo.io/themes/installing/) for more information on installing the Theme.

```bash
$ mkdir themes
$ git submodule add --branch custom https://gitlab.com/drmaxx/beautifulhugo-customized.git themes/beautifulhugo-customized
```

### Install as submodule

```bash
$ mkdir themes
$ git submodule add --branch custom https://gitlab.com/drmaxx/beautifulhugo-customized.git themes/beautifulhugo-customized
```

## Extra Features

### Responsive

This theme is designed to look great on both large-screen and small-screen (mobile) devices.

### Syntax highlighting

This theme has support for either Hugo's lightning fast Chroma, or both server side and client side highlighting. See [the Hugo docs for more](https://gohugo.io/content-management/syntax-highlighting/).

#### Chroma - New server side syntax highlighting

To enable Chroma, add the following to your site parameters:

```toml
pygmentsCodeFences = true
pygmentsUseClasses = true
```

Then, you can generate a different style by running:

```bash
hugo gen chromastyles --style=trac > static/css/syntax.css
```

#### Pygments - Old server side syntax highlighting

To use this feature install Pygments (`pip install Pygments`) and add the following to your site parameters:

```toml
pygmentsStyle      = "trac"
pygmentsUseClassic = true
```

Pygments is mostly compatable with the newer Chroma. It is slower but has some additional theme options. I recommend Chroma over Pygments. Pygments will use `syntax.css` for highlighting, unless you also set the config `pygmentsUseClasses = false` which will generate the style code directly in the HTML file.

#### Highlight.js - Client side syntax highlighting

```toml
[Params]
    useHLJS = true
```

Client side highlighting does not require pygments to be installed. This will use `highlight.min.css` instead of `syntax.css` for highlighting (effectively disabling Chroma). Highlight.js has a wider range of support for languages and themes, and an alternative highlighting engine.

### Disqus support

To use this feature, uncomment and fill out the `disqusShortname` parameter in `config.toml`.

### Staticman support

Add _Staticman_ configuration section in `config.toml` or `config.yaml`

Sample `config.toml` configuration

```toml
[Params.staticman]
  api     = "https://<API-ENDPOINT>/v3/entry/{GIT-HOST}/<USERNAME>/<REPOSITORY-BLOGNAME>/master/comments"
[Params.staticman.recaptcha]
  sitekey = "xa9mahh9iej4Thahkah6aephat4ohngoo8enohY3"
  secret  = "EiD1NooCa8eanoo6noo1aixaphei2at6ahw8nuemoo1juax4Oniechat3vahtaiLoRaon2oosaideeTeez5zovaephieboi0Queengaengeey5fa9ih0wuwoo5liech3eeKohRoh2Phain1Leech8d="
```

Note: The public `API-ENDPOINT` https://staticman.net is currently hitting its API limit, so one may use other API instances to provide Staticman comment service.

The section `[Params.staticman.recaptcha]` is _optional_. To add reCAPTCHA to your site, you have to replace the default values with your own ones (to be obtained from Google.) The site `secret` has to be encrypted with

```
https://<API-ENDPOINT>/v3/encrypt/<SITE-SECRET>
```

You must also configure the `staticman.yml` in you blog website.

```yaml
comments:
  allowedFields: ["name", "email", "website", "comment"]
  branch: "master"
  commitMessage: "New comment in {options.slug}"
  path: "data/comments/{options.slug}"
  filename: "comment-{@timestamp}"
  format: "yaml"
  moderation: true
  requiredFields: ["name", "email", "comment"]
  transforms:
    email: md5
  generatedFields:
    date:
      type: "date"
      options:
        format: "iso8601"
  reCaptcha:
    enabled: true
    siteKey: "6LeGeTgUAAAAAAqVrfTwox1kJQFdWl-mLzKasV0v"
    secret: "hsGjWtWHR4HK4pT7cUsWTArJdZDxxE2pkdg/ArwCguqYQrhuubjj3RS9C5qa8xu4cx/Y9EwHwAMEeXPCZbLR9eW1K9LshissvNcYFfC/b8KKb4deH4V1+oqJEk/JcoK6jp6Rr2nZV4rjDP9M7nunC3WR5UGwMIYb8kKhur9pAic="
```

If you _don't_ have the section `[Params.staticman]` in `config.toml`, you _won't_ need the section `reCaptcha` in `staticman.yml`

### Google Analytics

To add Google Analytics, simply sign up to [Google Analytics](https://www.google.com/analytics/) to obtain your Google Tracking ID, and add this tracking ID to the `googleAnalytics` parameter in `config.toml`.

### Commit SHA on the footer

If the source of your site is in a Git repo, the SHA corresponding to the commit the site is built from can be shown on the footer. To do so, two site parameters `commit` has to be defined in the config file `config.toml`:

```toml
enableGitInfo = true
[Params]
  commit = "https://github.com/<username>/<siterepo>/tree/"
```

See at [vincenttam/vincenttam.gitlab.io](https://gitlab.com/vincenttam/vincenttam.gitlab.io) for an example of how to add it to a continuous integration system.

### Multilingual

To allow Beautifulhugo Customized to go multilingual, you need to define the languages
you want to use inside the `languages` parameter on `config.toml` file, also
redefining the content dir for each one. Check the `i18n/` folder to see all
languages available.

```toml
[languages]
  [languages.en]
    contentDir = "content/en" # English
  [languages.ja]
    contentDir = "content/ja" # Japanese
  [languages.br]
    contentDir = "content/br" # Brazilian Portuguese
```

Now you just need to create a subdir within the `content/` folder for each
language and just put stuff inside `page/` and `post/` regular directories.

```text
content/      content/      content/
└── en/       └── br/       └── ja/
    ├── page/     ├── page/     ├── page/
    └── post/     └── post/     └── post/
```

### Extra shortcodes

There are two extra shortcodes provided (along with the customized figure shortcode):

#### Details

This simply adds the html5 detail attribute, supported on all _modern_ browsers. Use it like this:

```md
{{< details "This is the details title (click to expand)" >}}
This is the content (hidden until clicked).
{{< /details >}}
```

#### Split

This adds a two column side-by-side environment (will turn into 1 col for narrow devices):

```html
{{< columns >}}
This is column 1.
{{< column >}}
This is column 2.
{{< endcolumns >}}
```

## About

This is my version of the Beautifulhugo Theme to learn how Theme creating works and also to adjust several aspects to fit my needs.

Most of this readme is not yet adjusted and contains the original text, please don't blame me for that.

This was an adaptation of the Jekyll theme [Beautiful Jekyll](https://deanattali.com/beautiful-jekyll/) by [Dean Attali](https://deanattali.com/aboutme#contact) which was ported to [Beautifulhugo](https://github.com/halogenica/beautifulhugo) by [halogenica](https://github.com/halogenica). It supports most of the features of the original theme, and many new features. It has diverged from the Jekyll theme over time, with years of community updates.

## License

MIT Licensed, see [LICENSE](LICENSE).
